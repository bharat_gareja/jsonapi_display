import { Button, JsonApi } from './libs';
import './index.css';
function App() {
  return (
    <>
      <Button name={'Submit'} />
      <JsonApi />
    </>
  );
}

export default App;

