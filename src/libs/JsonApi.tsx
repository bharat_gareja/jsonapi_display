import axios from 'axios';
import React, { useState, ChangeEvent } from 'react';

export const JsonApi: React.FC = () => {
  const [input, setInput] = useState<string | null>(null);
  const [data, setData] = useState<any[]>([]);

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setInput(e.target.value);
  };

  const handleSubmitButton = async () => {
    try {
      if (input) {
        const response = await axios.get(input);
        setData(response.data);
      }
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <>
      <div className='flex items-center  flex-col my-3'>
        <h1 className='my-3 text-3xl font-semibold'>Enter Api</h1>
        <input
          type='text'
          className='border px-2 py-2 w-1/3 rounded-lg focus:ring-blue-500 focus:border-blue-500 text-gray-900 border-gray-300'
          placeholder='Enter your api keys '
          onChange={(e) => handleInputChange(e)}
        />
        <button
          onClick={() => handleSubmitButton()}
          className=' px-2 py-1 border  rounded-lg bg-slate-500 text-white my-2 hover:bg-slate-800'
        >
          Submit
        </button>
      </div>
      {data.length > 0 && <pre>{JSON.stringify(data, null, 2)}</pre>}
    </>
  );
};
