import React from 'react';
interface buttonType {
  name: string;
}
const Button: React.FC<buttonType> = ({ name }) => {
  return <button>{name}</button>;
};

export default Button;
