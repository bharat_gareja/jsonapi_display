# ApiLibrary React Component

`ApiLibrary` is a simple React component for making API requests and displaying the response data.

## Installation

You can install `ApiLibrary` using npm:

```bash
npm install  ApiLibrary
```

Usage
To use ApiLibrary in your React application, import it and include it in your JSX code.
import React from 'react';
import {JsonApi} from 'jsonapi-display';

function App() {
return (

<div>
<JsonApi />
</div>
);
}

export default App;

Props
ApiLibrary does not accept any props.

Features
Allows users to input an API URL.
Fetches data from the provided API URL using Axios.
Displays the API response data in a JSON format.

Development
If you want to contribute to the development of ApiLibrary, you can clone the repository and set up the development environment as follows:

1.  Clone the repository:
    git clone https://github.com/bharat_gareja/apilibrary.git

2.  Install the dependencies
    cd apilibrary
    npm install

3.  Start the development server:
    npm start

